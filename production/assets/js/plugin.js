/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/collapse
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.fn.collapse = function(options) {

        var defaults = {
            debug:      false
        };
     
        var settings = $.extend( {}, defaults, options );

        this.each(function() {
            var self = $(this);

            self.on('click', function() {
                var selector = $(this).data('collapse');
                $(selector).toggle();
            });
        });

        return this;
    };

}( jQuery ));