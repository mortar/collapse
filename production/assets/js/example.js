/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/collapse
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
$('[data-collapse]').collapse({
	debug: 		true
});